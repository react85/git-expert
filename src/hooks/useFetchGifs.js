import { useState, useEffect } from 'react'
import { getGifs } from '../helpers/getGifs';

export const useFetchGifs = (category) => {
    const [state, setState] = useState({
        data: [],
        loading: true
    });

    // en la lista de dependencias se le dice que sólo se ejecute la primera vez.
    // Los effects no pueden ser async
    useEffect(() => {
        getGifs(category)
            .then(imgs => {
                setTimeout(() => {
                    setState({
                        data: imgs,
                        loading: false
                    });
                }, 2000);
            });

    }, [category]);

    return state;
}
