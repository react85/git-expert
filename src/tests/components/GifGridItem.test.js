import React from 'react';
import { shallow } from 'enzyme';
import { GifGridItem } from '../../components/GifGridItem';

describe('Tests on <GifGridItem />', () => {

    const title = 'Title';
    const url = 'http://something-image.com';
    const wrapper = shallow(<GifGridItem title={title} url={url}></GifGridItem>);

    test('should show the component correctly', () => {
        expect(wrapper).toMatchSnapshot();
    });

    test('should have a paragraph with title', () => {
        const p = wrapper.find('p');
        expect(p.text().trim()).toBe(title);
    });

    test('should have an img with src like url and alt', () => {
        const img = wrapper.find('img');
        expect(img.prop('src')).toBe(url);
        expect(img.prop('alt')).toBe(title);
    });

    test('should have animate__bounce class', () => {
        const div = wrapper.find('div');
        const className = div.prop('className');

        expect(className.includes('animate__bounce')).toBe(true);
    });




});
