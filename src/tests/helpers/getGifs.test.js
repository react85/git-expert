import { getGifs } from '../../helpers/getGifs';

describe('Tests on getGifs Fetch', () => {

    test('should get 10 elements', async () => {
        const gifs = await getGifs('Dragon ball');
        expect(gifs.length).toBe(10);
    });

    test('should get 0 elements when text was empty', async () => {
        const gifs = await getGifs('');
        expect(gifs.length).toBe(0);
    });
});
